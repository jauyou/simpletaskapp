//
//  ServerAPI.h
//  SimpleTaskApp
//
//  Created by ChenChao-Yu on 2017/1/17.
//  Copyright © 2017年 ChenChao-Yu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TaskModel.h"

@interface ServerAPI : NSObject
@property NSString *const URLString;
+ (void)addTaskToServer:(NSString *)dateTime
                   task:(NSString *)task
               isFinish:(BOOL)isFinish
              isInQueue:(BOOL)isInQueue
               callback:(void (^)(void))callback;
+ (void)getTaskListFromServer:(void (^)(NSArray <TaskModel *> *))callback;
+ (void)updateTaskToServer:(NSString *)dateTime
                      task:(NSString *)taskDesc
                  isFinish:(BOOL)isFinish
                 isInQueue:(BOOL)isInQueue
                  callback:(void (^)(void))callback;
+ (void)deleteTaskToServer:(NSString *)dateTime
                      task:(NSString *)taskDesc
                  isFinish:(BOOL)isFinish
                 isInQueue:(BOOL)isInQueue
                  callback:(void (^)(void))callback;
@end
