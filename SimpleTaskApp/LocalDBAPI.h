//
//  LocalDBAPI.h
//  SimpleTaskApp
//
//  Created by ChenChao-Yu on 2017/1/17.
//  Copyright © 2017年 ChenChao-Yu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TaskEntityMO+CoreDataClass.h"
#import "DeleteTaskEntityMO+CoreDataClass.h"
#import "ModifyTaskEntityMO+CoreDataClass.h"
#import "UploadTaskEntityMO+CoreDataClass.h"

@interface LocalDBAPI : NSObject
+ (void)addTaskToLocal:(NSString *)datetime
                  task:(NSString *)task
              isFinish:(BOOL)isFinish;
+ (NSArray<TaskEntityMO *> *)getTaskListFromLocal;
+ (void)updateTask:(TaskEntityMO *)task datetime:(NSString *)datetime taskDesc:(NSString *)taskDesc isFinish:(BOOL)isFinish;
+ (void)removeTask:(TaskEntityMO *)task;

+ (void)addTaskToUploadQueue:(NSString *)datetime
                        task:(NSString *)task
                    isFinish:(BOOL)isFinish;
+ (NSArray<UploadTaskEntityMO *> *)getTaskListFromUploadQueue;
+ (void)removeUploadQueueTask:(UploadTaskEntityMO *)task;

+ (void)addTaskToModifyQueue:(NSString *)datetime
                        task:(NSString *)task
                    isFinish:(BOOL)isFinish;
+ (NSArray<ModifyTaskEntityMO *> *)getTaskListFromModifyQueue;
+ (void)removeModifyQueueTask:(ModifyTaskEntityMO *)task;
+ (void)addTaskToDeleteQueue:(NSString *)datetime
                        task:(NSString *)task
                    isFinish:(BOOL)isFinish;
+ (NSArray<DeleteTaskEntityMO *> *)getTaskListFromDeleteQueue;
+ (void)removeDeleteQueueTask:(DeleteTaskEntityMO *)task;
@end
