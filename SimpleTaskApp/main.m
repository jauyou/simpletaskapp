//
//  main.m
//  SimpleTaskApp
//
//  Created by ChenChao-Yu on 2017/1/17.
//  Copyright © 2017年 ChenChao-Yu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
