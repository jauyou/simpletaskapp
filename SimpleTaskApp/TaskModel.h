//
//  TaskModel.h
//  SimpleTaskApp
//
//  Created by ChenChao-Yu on 2017/1/17.
//  Copyright © 2017年 ChenChao-Yu. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface TaskModel : JSONModel
@property (nonatomic) NSString *datetime;
@property (nonatomic) NSString *task;
@property (nonatomic) BOOL isFinish;
@end
