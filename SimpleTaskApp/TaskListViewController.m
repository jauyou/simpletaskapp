//
//  TaskListViewController.m
//  SimpleTaskApp
//
//  Created by ChenChao-Yu on 2017/1/17.
//  Copyright © 2017年 ChenChao-Yu. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "AFNetworking/AFNetworking.h"
#import "AppDelegate.h"
#import "EditTaskViewController.h"
#import "LocalDBAPI.h"
#import "ServerAPI.h"
#import "TaskEntityMO+CoreDataClass.h"
#import "TaskListViewController.h"
#import "TaskModel.h"

@interface TaskListViewController ()

@end

@implementation TaskListViewController
{
    NSMutableArray *tasks;
    NSMutableArray *searchResults;
    NSInteger row;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // detect network status change
    
    [LocalDBAPI getTaskListFromLocal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        
        if (status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi)
        {
            [self syncClient];
        } else {
            [self syncOffline];
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    // check local list if empty
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - server-client-sync
- (void) syncClient
{
    NSArray<TaskEntityMO *> *localTaskList = [LocalDBAPI getTaskListFromLocal];
    // NSArray<ModifyTaskEntityMO *> *modifyTaskList = [LocalDBAPI getTaskListFromLocal];
    // NSArray<DeleteTaskEntityMO *> *deleteTaskList = [LocalDBAPI getTaskListFromLocal];
    
    if ([localTaskList count] == 0) {
        [ServerAPI getTaskListFromServer:^(NSArray<TaskModel *> *array)
         {
             // store task to local
             for(TaskModel *task in array)
             {
                 if ([task.datetime length] != 0 && [task.task length] != 0)
                 {
                     [LocalDBAPI addTaskToLocal:task.datetime task:task.task isFinish:task.isFinish];
                 }
             }
             NSArray<TaskEntityMO *> *localTaskList = [LocalDBAPI getTaskListFromLocal];
             tasks = [[NSMutableArray alloc] initWithArray:localTaskList];
             
             dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
             dispatch_async(mainThreadQueue, ^{
                 
                 [_taskTableView reloadData]; // Update table UI
             });
         }];
    } else {
        // check every task in server is in local or delete queue [test case : offline add a task, then online]
        // add server task to local task
        /*
        [ServerAPI getTaskListFromServer:^(NSArray<TaskModel *> *array) {
            // store task to local
            
            for(TaskModel *serverTask in array)
            {
                BOOL matchTask = NO;
                for (TaskModel *localTask in localTaskList)
                {
                    BOOL dateEqual = [serverTask.datetime isEqualToString:localTask.datetime];
                    BOOL taskEqual = [serverTask.task isEqualToString:localTask.task];
                    BOOL isFinishEqual = !(serverTask.isFinish ^ localTask.isFinish);
                    if (dateEqual & taskEqual & isFinishEqual)
                    {
                        matchTask = YES;
                    }
                }
                if(!matchTask)
                {
                    [LocalDBAPI addTaskToLocal:serverTask.datetime task:serverTask.task isFinish:serverTask.isFinish];
                }
            }
            
            NSArray<TaskEntityMO *> *localTaskList = [LocalDBAPI getTaskListFromLocal];
            tasks = [[NSMutableArray alloc] initWithArray:localTaskList];
            
            dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
            dispatch_async(mainThreadQueue, ^{
                
                [_taskTableView reloadData]; // Update table UI
            });
        }];
        */
        [self syncServer];
        tasks = [[NSMutableArray alloc] initWithArray:localTaskList];
        [_taskTableView reloadData];
    }
}

- (void) syncServer
{
    // step 1
    // upload local task in upload queue
    NSArray<UploadTaskEntityMO *> *uploadTaskList = [LocalDBAPI getTaskListFromUploadQueue];
    
    for (UploadTaskEntityMO *uploadTask in uploadTaskList)
    {
        // api can't be called Simultaneously
        UploadTaskEntityMO __block *task = uploadTask;
        [ServerAPI addTaskToServer:task.datetime task:task.task isFinish:task.isFinish isInQueue:YES callback:^(void) {
            [LocalDBAPI removeUploadQueueTask:uploadTask];
        }];
        sleep(3);
    }

    // step 2
    // modify server task in modify queue
    NSArray<ModifyTaskEntityMO *> *modifyTaskList = [LocalDBAPI getTaskListFromModifyQueue];
    
    for (ModifyTaskEntityMO *modifyTask in modifyTaskList)
    {
        // api can't be called Simultaneously
        ModifyTaskEntityMO __block *task = modifyTask;
        [ServerAPI updateTaskToServer:task.datetime task:task.task isFinish:task.isFinish isInQueue:YES callback:^(void) {
             [LocalDBAPI removeModifyQueueTask:modifyTask];
             NSLog(@"queue = %@", [LocalDBAPI getTaskListFromUploadQueue]);
        }];
        sleep(3);
    }

    // step 3
    // TODO delete server task in delete queue
    NSArray<DeleteTaskEntityMO *> *deleteTaskList = [LocalDBAPI getTaskListFromDeleteQueue];
    
    for (DeleteTaskEntityMO *deleteTask in deleteTaskList)
    {
        // api can't be called Simultaneously
        DeleteTaskEntityMO __block *task = deleteTask;
        [ServerAPI deleteTaskToServer:task.datetime task:task.task isFinish:task.isFinish isInQueue:YES callback:^(void) {
             [LocalDBAPI removeDeleteQueueTask:deleteTask];
             NSLog(@"queue = %@", [LocalDBAPI getTaskListFromDeleteQueue]);
        }];
        sleep(3);
    }
}

- (void)syncOffline
{
    NSArray<TaskEntityMO *> *localTaskList = [LocalDBAPI getTaskListFromLocal];
    tasks = [[NSMutableArray alloc] initWithArray:localTaskList];
    [_taskTableView reloadData];
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [searchResults count];
    }
    else
    {
        return [tasks count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * taskCellIdentifier = @"TaskCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:taskCellIdentifier];
    if (cell == nil)
    {
        // Create a cell to display an ingredient.
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:taskCellIdentifier];
    }
    TaskEntityMO *task;
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        task = [searchResults objectAtIndex:indexPath.row];
    }
    else
    {
        task = [tasks objectAtIndex:indexPath.row];
    }
    
    // Configure the cell.
    cell.textLabel.text = [task valueForKey:@"datetime"];
    cell.detailTextLabel.text = [task valueForKey:@"task"];
    
    BOOL isFinish = [((NSNumber *)[task valueForKey:@"isFinish"]) boolValue];
    if (isFinish) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TaskEntityMO *task;
    row = indexPath.row;
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        task = [searchResults objectAtIndex:indexPath.row];
    }
    else
    {
        task = [tasks objectAtIndex:indexPath.row];
    }
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Task"
                                          message:task.task
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
    UIAlertAction *editAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Edit", @"Edit task")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Edit task");
                                       [self performSegueWithIdentifier: @"editTaskSegue" sender: self];
                                   }];
    UIAlertAction *deleteAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Delete", @"Delete task")
                                    style:UIAlertActionStyleDestructive
                                    handler:^(UIAlertAction *action)
                                    {
                                        NSLog(@"Delete task");
                                        [ServerAPI deleteTaskToServer:task.datetime task:task.task isFinish:task.isFinish isInQueue:NO callback:nil];
                                        [LocalDBAPI removeTask:task];
                                        NSArray<TaskEntityMO *> *localTaskList = [LocalDBAPI getTaskListFromLocal];
                                        tasks = [[NSMutableArray alloc] initWithArray:localTaskList];
                                        dispatch_queue_t mainThreadQueue = dispatch_get_main_queue();
                                        dispatch_async(mainThreadQueue, ^{
                                            
                                            [_taskTableView reloadData]; // Update table UI
                                        });
                                    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:editAction];
    [alertController addAction:deleteAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - search
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    searchResults = [[NSMutableArray alloc] init];
    for (TaskModel *task in tasks) {
        if ([task.datetime containsString:searchText] || [task.task containsString:searchText])
        {
            [searchResults addObject:task];
        }
    }
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

#pragma mark - segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"createTaskSegue"])
    {
        EditTaskViewController *destViewController = (EditTaskViewController *)segue.destinationViewController;
        destViewController.isNewTask = YES;
    }
    
    if ([[segue identifier] isEqualToString:@"editTaskSegue"]) {
        
        TaskEntityMO *task;
        if (_taskTableView == self.searchDisplayController.searchResultsTableView)
        {
            task = [searchResults objectAtIndex:[[_taskTableView indexPathForSelectedRow] row]];
        }
        else
        {
            NSLog(@"%ld", row);
            task = [tasks objectAtIndex:row];
        }

        EditTaskViewController *destViewController = segue.destinationViewController;
        destViewController.isNewTask = NO;
        destViewController.task = task;
    }
}


@end
