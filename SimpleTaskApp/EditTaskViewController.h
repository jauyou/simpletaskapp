//
//  EditTaskViewController.h
//  SimpleTaskApp
//
//  Created by ChenChao-Yu on 2017/1/17.
//  Copyright © 2017年 ChenChao-Yu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaskEntityMO+CoreDataClass.h"

@interface EditTaskViewController : UIViewController
@property(nonatomic) BOOL isNewTask;
@property(nonatomic) TaskEntityMO *task;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UISwitch *isFinishUISwitch;
- (IBAction)uneditTask:(id)sender;
- (IBAction)updateTask:(id)sender;

@end
