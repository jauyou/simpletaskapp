//
//  ServerAPI.m
//  SimpleTaskApp
//
//  Created by ChenChao-Yu on 2017/1/17.
//  Copyright © 2017年 ChenChao-Yu. All rights reserved.
//

#import "ServerAPI.h"

#import "AFNetworking/AFNetworking.h"
#import "LocalDBAPI.h"

@implementation ServerAPI
@synthesize URLString;
NSString *const URLString = @"https://sheetsu.com/apis/v1.0/68a85ede4b79";

#pragma mark - Server APIs
+ (void)addTaskToServer:(NSString *)dateTime
                   task:(NSString *)taskDesc
               isFinish:(BOOL)isFinish
              isInQueue:(BOOL)isInQueue
               callback:(void (^)(void))callback
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *parametersDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          dateTime, @"datetime",
                                          taskDesc, @"task",
                                          isFinish?@"TRUE":@"FALSE", @"isFinish", nil];
    
    [manager POST:URLString parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success!");
        if (isInQueue) {
            callback();
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        // push to upload queue retry when network is available
        if (!isInQueue) {
            [LocalDBAPI addTaskToUploadQueue:dateTime task:taskDesc isFinish:isFinish];
        }
    }];
}

+ (void)getTaskListFromServer:(void (^)(NSArray <TaskModel *> *))callback {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:URLString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSError *error;
        NSArray <TaskModel *> *arrays = [TaskModel arrayOfModelsFromDictionaries:responseObject error:&error];
        NSLog(@"Array = %@", arrays);
        
        callback(arrays);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

+ (void)updateTaskToServer:(NSString *)dateTime
                   task:(NSString *)taskDesc
               isFinish:(BOOL)isFinish
              isInQueue:(BOOL)isInQueue
               callback:(void (^)(void))callback
{
    NSString *putURL = [NSString stringWithFormat:@"%@/datetime/%@", URLString, dateTime];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *parametersDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          dateTime, @"datetime",
                                          taskDesc, @"task",
                                          isFinish?@"TRUE":@"FALSE", @"isFinish", nil];
    
    [manager PUT:putURL parameters:parametersDictionary success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success!");
        if (isInQueue) {
            callback();
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        // push to upload queue retry when network is available
        // TODO
        if (!isInQueue) {
            [LocalDBAPI addTaskToModifyQueue:dateTime task:taskDesc isFinish:isFinish];
        }
    }];
}

+ (void)deleteTaskToServer:(NSString *)dateTime
                      task:(NSString *)taskDesc
                  isFinish:(BOOL)isFinish
                 isInQueue:(BOOL)isInQueue
                  callback:(void (^)(void))callback
{
    NSString *deleteURL = [NSString stringWithFormat:@"%@/datetime/%@", URLString, dateTime];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager DELETE:deleteURL parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success!");
        if (isInQueue) {
            callback();
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        // push to upload queue retry when network is available
        if (!isInQueue) {
            [LocalDBAPI addTaskToDeleteQueue:dateTime task:taskDesc isFinish:isFinish];
            [LocalDBAPI getTaskListFromDeleteQueue];
        }
    }];
}

@end
