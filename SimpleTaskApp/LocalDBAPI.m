//
//  LocalDBAPI.m
//  SimpleTaskApp
//
//  Created by ChenChao-Yu on 2017/1/17.
//  Copyright © 2017年 ChenChao-Yu. All rights reserved.
//

#import "LocalDBAPI.h"
#import "AppDelegate.h"

@implementation LocalDBAPI

#pragma mark - local db

+ (void)addTaskToLocal:(NSString *)datetime
                  task:(NSString *)task
              isFinish:(BOOL)isFinish
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    
    NSManagedObject *entityNameObj = [NSEntityDescription insertNewObjectForEntityForName:@"TaskEntity" inManagedObjectContext:context];
    [entityNameObj setValue:datetime forKey:@"datetime"];
    [entityNameObj setValue:task forKey:@"task"];
    [entityNameObj setValue:[NSNumber numberWithBool:isFinish] forKey:@"isFinish"];
    
    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) saveContext];
}

+ (NSArray<TaskEntityMO *> *)getTaskListFromLocal
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    NSFetchRequest<TaskEntityMO *> *fetchRequest = [TaskEntityMO fetchRequest];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"datetime" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    NSError *error ;
    NSArray *resultArray= [context executeFetchRequest:fetchRequest error:&error];
    if (error != nil)
    {
        // fetch data fail
        return [[NSArray alloc] init];
    }
    return resultArray;
}

+ (void)removeTask:(TaskEntityMO *)task
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    [context deleteObject:task];
    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) saveContext];
}

#pragma mark - upload queue

+ (void)addTaskToUploadQueue:(NSString *)datetime
                        task:(NSString *)task
                    isFinish:(BOOL)isFinish
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    
    NSManagedObject *entityNameObj = [NSEntityDescription insertNewObjectForEntityForName:@"UploadTaskEntity" inManagedObjectContext:context];
    [entityNameObj setValue:datetime forKey:@"datetime"];
    [entityNameObj setValue:task forKey:@"task"];
    [entityNameObj setValue:[NSNumber numberWithBool:isFinish] forKey:@"isFinish"];
    
    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) saveContext];
}

+ (NSArray<UploadTaskEntityMO *> *)getTaskListFromUploadQueue
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    NSFetchRequest<UploadTaskEntityMO *> *fetchRequest = [UploadTaskEntityMO fetchRequest];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"datetime" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    NSError *error ;
    NSArray *resultArray= [context executeFetchRequest:fetchRequest error:&error];
    if (error != nil)
    {
        // fetch data fail
        return [[NSArray alloc] init];
    }
    NSLog(@"queue = %@", resultArray);
    return resultArray;
}

+ (void)updateTask:(TaskEntityMO *)task datetime:(NSString *)datetime taskDesc:(NSString *)taskDesc isFinish:(BOOL)isFinish
{
    [task setValue:datetime forKey:@"datetime"];
    [task setValue:taskDesc forKey:@"task"];
    [task setValue:[NSNumber numberWithBool:isFinish] forKey:@"isFinish"];
}

+ (void)removeUploadQueueTask:(UploadTaskEntityMO *)task
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    [context deleteObject:task];
    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) saveContext];
}

#pragma mark - modify queue

+ (void)addTaskToModifyQueue:(NSString *)datetime
                        task:(NSString *)task
                    isFinish:(BOOL)isFinish
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    
    NSManagedObject *entityNameObj = [NSEntityDescription insertNewObjectForEntityForName:@"ModifyTaskEntity" inManagedObjectContext:context];
    [entityNameObj setValue:datetime forKey:@"datetime"];
    [entityNameObj setValue:task forKey:@"task"];
    [entityNameObj setValue:[NSNumber numberWithBool:isFinish] forKey:@"isFinish"];
    
    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) saveContext];
}

+ (NSArray<ModifyTaskEntityMO *> *)getTaskListFromModifyQueue
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    NSFetchRequest<ModifyTaskEntityMO *> *fetchRequest = [ModifyTaskEntityMO fetchRequest];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"datetime" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    NSError *error ;
    NSArray *resultArray= [context executeFetchRequest:fetchRequest error:&error];
    if (error != nil)
    {
        // fetch data fail
        return [[NSArray alloc] init];
    }
    NSLog(@"queue = %@", resultArray);
    return resultArray;
}

+ (void)removeModifyQueueTask:(ModifyTaskEntityMO *)task
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    [context deleteObject:task];
    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) saveContext];
}

#pragma mark - delete queue

+ (void)addTaskToDeleteQueue:(NSString *)datetime
                        task:(NSString *)task
                    isFinish:(BOOL)isFinish
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    
    NSManagedObject *entityNameObj = [NSEntityDescription insertNewObjectForEntityForName:@"DeleteTaskEntity" inManagedObjectContext:context];
    [entityNameObj setValue:datetime forKey:@"datetime"];
    [entityNameObj setValue:task forKey:@"task"];
    [entityNameObj setValue:[NSNumber numberWithBool:isFinish] forKey:@"isFinish"];
    
    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) saveContext];
}

+ (NSArray<DeleteTaskEntityMO *> *)getTaskListFromDeleteQueue
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    NSFetchRequest<DeleteTaskEntityMO *> *fetchRequest = [DeleteTaskEntityMO fetchRequest];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"datetime" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    NSError *error ;
    NSArray *resultArray= [context executeFetchRequest:fetchRequest error:&error];
    if (error != nil)
    {
        // fetch data fail
        return [[NSArray alloc] init];
    }
    NSLog(@"queue = %@", resultArray);
    return resultArray;
}

+ (void)removeDeleteQueueTask:(DeleteTaskEntityMO *)task
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    [context deleteObject:task];
    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) saveContext];
}
@end
