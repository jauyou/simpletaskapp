//
//  EditTaskViewController.m
//  SimpleTaskApp
//
//  Created by ChenChao-Yu on 2017/1/17.
//  Copyright © 2017年 ChenChao-Yu. All rights reserved.
//

#import "EditTaskViewController.h"
#import "LocalDBAPI.h"
#import "ServerAPI.h"

@interface EditTaskViewController ()

@end

@implementation EditTaskViewController
@synthesize isNewTask;
@synthesize task;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (!isNewTask) {
        _dateLabel.text = task.datetime;
        [_isFinishUISwitch setOn:task.isFinish];
        _descriptionTextView.text = task.task;
    } else {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"[yyyy-MM-DD]HH:mm:ss"];

        NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
        _dateLabel.text = stringFromDate;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)uneditTask:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)updateTask:(id)sender {
    if ([_dateLabel.text length] == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Date field is empty"
                                                                       message:@"Please fill date before you sumbit."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             
                                                         }];
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if([_descriptionTextView.text length] == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Description field is empty"
                                                                       message:@"Please fill date before you sumbit."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             
                                                         }];
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        if (isNewTask) {
            // add a task
            [LocalDBAPI addTaskToLocal:_dateLabel.text task:_descriptionTextView.text isFinish:[_isFinishUISwitch isOn]];
            [ServerAPI addTaskToServer:_dateLabel.text task:_descriptionTextView.text isFinish:[_isFinishUISwitch isOn] isInQueue:NO callback:nil];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            // update a task
            [LocalDBAPI updateTask:task datetime:_dateLabel.text taskDesc:_descriptionTextView.text isFinish:[_isFinishUISwitch isOn]];

            [ServerAPI updateTaskToServer:task.datetime task:task.task isFinish:task.isFinish isInQueue:NO callback:nil];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

@end
